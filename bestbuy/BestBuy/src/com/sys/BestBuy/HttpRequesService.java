package com.sys.BestBuy;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class HttpRequesService extends IntentService {
	RequestMaker request_maker_obj = new RequestMaker();
	QueryMaker query_maker_obj = new QueryMaker();
	String result, query;

	public HttpRequesService() {
		super("HttpRequesService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String identifier = intent.getStringExtra("identifier");
		String value = intent.getStringExtra("value");
		query = query_maker_obj.make_query(identifier, value);
		// Log.e("query formed", query);
		result = request_maker_obj.get_Http_Response(query);
		String[] Department_name = new JsonParser().Parse_json(result);
		if (Department_name[0] == "") {
			identifier = "product";
			query = query_maker_obj.make_query(identifier, value);
			result = request_maker_obj.get_Http_Response(query);
		}

		Intent broadcastIntent = new Intent("com.sys.BestBuy");
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra("result", result);
		broadcastIntent.putExtra("identifier", identifier);
		sendBroadcast(broadcastIntent);
		stopSelf();
	}
}
