package com.sys.BestBuy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {
	public String[] Parse_json(String result) {
		try {
			JSONArray json = new JSONObject(result).getJSONArray("categories")
					.getJSONObject(0).getJSONArray("subCategories");
			if (json.length() == 0) {
				String[] Department_name = { "" };
				return Department_name;

			}
			String Department_name[] = new String[json.length()];
			for (int i = 0; i < json.length(); i++) {
				JSONObject json4 = json.getJSONObject(i);
				Department_name[i] = json4.getString("name");
			}
			return Department_name;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String[] Parse_json1(String result) {
		try {
			JSONArray json = new JSONObject(result).getJSONArray("products");
			String Department_name[] = new String[json.length()];
			for (int i = 0; i < json.length(); i++) {
				JSONObject json4 = json.getJSONObject(i);
				Department_name[i] = json4.getString("name");
			}
			return Department_name;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String[] get_product_sku(String result) {
		try {
			JSONArray json = new JSONObject(result).getJSONArray("products");
			String sku[] = new String[json.length()];
			for (int i = 0; i < json.length(); i++) {
				JSONObject json4 = json.getJSONObject(i);
				sku[i] = json4.getString("sku");
			}
			return sku;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
