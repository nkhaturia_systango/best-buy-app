package com.sys.BestBuy;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class ProductReviewActivity extends ListActivity {
	String product_sku;
	String query, result;
	TextView comment, id, sku, title;

	RequestMaker request_maker_obj = new RequestMaker();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		product_sku = getIntent().getExtras().getString("product_sku");
		QueryMaker query_maker_obj = new QueryMaker();
		query = query_maker_obj.make_query("product review", product_sku);
		new ReviewDownloader().execute(query);
	}

	public class ReviewDownloader extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			return request_maker_obj.get_Http_Response(params[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("this is the result", result);
			if (result.contains("{  \"from\": 1,  \"to\": 0,  \"total\": 0,  ")
					|| result
							.contains("{  \"error\": {    \"code\": 400,    \"status\": \"400 Bad Request\"")) {
				Toast.makeText(getApplicationContext(),
						"sorry no Rview available", Toast.LENGTH_LONG).show();
			} else {
				Gson gson = new Gson();
				try {

					JSONObject response = new JSONObject(result);
					JSONArray reviews = response.getJSONArray("reviews");
					ProductReview[] product_review_obj = new ProductReview[reviews
							.length()];
					for (int i = 0; i < reviews.length(); i++) {
						product_review_obj[i] = gson.fromJson(reviews
								.getJSONObject(i).toString(2),
								ProductReview.class);
					}
					ProductReviewAdapter product_review_adapter = new ProductReviewAdapter(
							getApplicationContext(), product_review_obj);

					setListAdapter(product_review_adapter);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

	}

}
