package com.sys.BestBuy;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class BestBuyActivity extends ListActivity {
	List<MyArrayAdapter> adapter_list = new ArrayList<MyArrayAdapter>();
	
	public static int listcount=0;
	public String identifier;
	String[] product_sku;
	int page_no = 1;
	static String clicked_item_name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = new Intent(this, HttpRequesService.class);
		make_service("first", "first", intent);
		IntentFilter filter = new IntentFilter("com.sys.BestBuy");
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		MyBroadcastReceive receiver = new MyBroadcastReceive();
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Object o = this.getListAdapter().getItem(position);
		clicked_item_name = o.toString();
		if (identifier.equals("product")) {
			Intent intent = new Intent(BestBuyActivity.this,
					Product_Details.class);
			intent.putExtra("product_sku", product_sku[position]);
			startActivity(intent);
			// make_service("product_sku", product_sku[position], intent);
		} else {
			Intent intent = new Intent(this, HttpRequesService.class);
			make_service("department", clicked_item_name, intent);
		}
	}

	@Override
	public void onBackPressed() {
		listcount--;
		if (listcount < 0) {
			listcount = 0;
		}
		if (identifier.equals("product"))
			identifier = "department";
		setListAdapter(adapter_list.get(listcount));
	}

	public class MyBroadcastReceive extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String[] Department_name = null;
			String responseString = intent.getStringExtra("result");
			identifier = intent.getStringExtra("identifier");
			if (identifier.equals("product")) {
				Department_name = new JsonParser().Parse_json1(responseString);
				product_sku = new JsonParser().get_product_sku(responseString);
			} else {
				Department_name = new JsonParser().Parse_json(responseString);
			}
			MyArrayAdapter adapter = new MyArrayAdapter(context,
					Department_name);
			setTitle(identifier + " List");
			listcount++;
			adapter_list.add(adapter);
			setListAdapter(adapter);
		}

	}

	private void make_service(String identifier, String value, Intent intent) {
		intent.putExtra("identifier", identifier);
		intent.putExtra("value", value);
		startService(intent);
	}

}