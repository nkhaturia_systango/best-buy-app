package com.sys.BestBuy;

import java.io.InputStream;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Product_Details extends Activity {
	QueryMaker query_maker_obj = new QueryMaker();
	RequestMaker request_maker_obj = new RequestMaker();
	TextView name;
	TextView sku;
	TextView saleprice;
	ImageView product_image;
	String product_sku, query, product_url;
	Bitmap bitmap;

	// int page_no=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_details);
		name = (TextView) findViewById(R.id.tv_name);
		sku = (TextView) findViewById(R.id.tv_sku);
		saleprice = (TextView) findViewById(R.id.tv_sale_price);
		product_image = (ImageView) findViewById(R.id.product_image);
		product_sku = getIntent().getExtras().getString("product_sku");
		query = query_maker_obj.make_query("product_sku", product_sku);
		new GetData().execute(query);
		Button reviews_button = (Button) findViewById(R.id.review);
		reviews_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent Review_intent = new Intent(getApplicationContext(),
						ProductReviewActivity.class);
				Review_intent.putExtra("product_sku", product_sku);
				startActivity(Review_intent);
			}
		});
	}

	public class LoadImage extends AsyncTask<String, String, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... args) {
			try {
				bitmap = BitmapFactory.decodeStream((InputStream) new URL(
						args[0]).getContent());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {
			if (image != null) {
				product_image.setImageBitmap(image);
			}
		}
	}

	public class GetData extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			return request_maker_obj.get_Http_Response(params[0]);

		}

		@Override
		protected void onPostExecute(String result) {
			try {
				super.onPostExecute(result);
				Gson gson = new Gson();
				JSONObject response = new JSONObject(result);
				JSONArray products = response.getJSONArray("products");
				Product product = gson.fromJson(products.getJSONObject(0)
						.toString(2), Product.class);
				name.setText(product.getName());
				saleprice.setText(product.getSalePrice());
				product_url = product.getImage();
				sku.setText(product.getSku());
				new LoadImage().execute(product_url);
			} catch (Exception e) {
				Log.e("error", e.getLocalizedMessage());
			}
		}

	}

}
