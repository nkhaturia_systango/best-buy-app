package com.sys.BestBuy;

public class ProductReview {
	String comment;
	// float rating;
	String sku;
	String title;

	// String id;
	public String getComment() {
		return this.comment;
	}

	/*
	 * public int getId(){ return this.id; }
	 */
	// public float getRating(){
	// return this.rating;
	// }
	/*
	 * public String getReviewer_name(){ return this.reviewer_name; }
	 */
	public String getSku() {
		return this.sku;
	}

	public String getTitle() {
		return this.title;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/*
	 * public void setId(int id){ this.id=id; }
	 */
	// public void setRating(float rating){
	// this.rating=rating;
	// }
	/*
	 * public void setReviewer_name(String reviewer_name){
	 * this.reviewer_name=reviewer_name; }
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
