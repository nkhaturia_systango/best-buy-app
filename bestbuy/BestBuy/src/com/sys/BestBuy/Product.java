package com.sys.BestBuy;

public class Product {
	private String name;
	String sku;
	String image;
	String salePrice;

	public String getSku() {
		return this.sku;
	}

	public String getName() {
		return this.name;
	}

	public String getImage() {
		return this.image;
	}

	public String getSalePrice() {
		return this.salePrice;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setSale_price(String salePrice) {
		this.salePrice = salePrice;
	}
}
