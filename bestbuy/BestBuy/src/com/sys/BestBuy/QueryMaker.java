package com.sys.BestBuy;

public class QueryMaker {
	String fixed_part = "http://api.remix.bestbuy.com/v1/";
	String api_key = "apiKey=ugptuphbfnes49duyhwfwgsq";
	String format = "format=json";
	int page = 1;

	String make_query(String identifier, String value) {
		if (identifier.equals("first") & value.equals("first"))
			return fixed_part + "categories(id=cat00000)?" + format
					+ "&show=subCategories&" + api_key;
		else if (identifier.equals("department")) {
			value = value.replaceAll(" ", "%20");
			return fixed_part + "categories(name=%22" + value + "%22)?"
					+ format + "&show=subCategories&" + api_key
					+ "&pageSize=20";
		} else if (identifier.equals("product")) {
			value = value.replaceAll(" ", "%20");
			return fixed_part + "products(categoryPath.name=" + value
					+ "&active=true)?" + format + "&show=name,sku&" + api_key
					+ "&pageSize=5&page=1";
		} else if (identifier.equals("product_sku")) {
			return fixed_part + "products(sku=" + value + "&active=true)?"
					+ format + "&show=name,sku,image,salePrice&" + api_key;
		} else if (identifier.equals("product review")) {
			return fixed_part + "reviews(sku=" + value + ")?" + format
					+ "&show=comment,title,sku&" + api_key;
		}
		return null;
	}
}